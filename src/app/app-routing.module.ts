import { ViewTasksComponent } from './UI/view-tasks/view-tasks.component';
import { AddTaskComponent } from './UI/add-task/add-task.component';
import { AddUsersComponent } from './UI/add-users/add-users.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddProjectComponent } from './UI/add-project/add-project.component';




const routes: Routes = [
  {path: 'addProject' , component: AddProjectComponent},
  {path: 'addUser' , component: AddUsersComponent},
  {path: 'addTask' , component: AddTaskComponent},
  {path: 'viewTasks' , component: ViewTasksComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
