import { Tasks } from './tasks';
import { Projects } from './projects';
export class Users {
    User_ID: number;
    First_Name: string;
    Last_Name: string;
    Employee_ID: string;
    Project_ID: number;
    Task_ID: number;
    Projects: Projects;
    Tasks: Tasks;
}
