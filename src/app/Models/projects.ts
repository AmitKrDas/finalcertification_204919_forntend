import { Users } from './users';
import { Tasks } from './tasks';
export class Projects {
    Project_ID: number;
    Project: string;
    Start_Date: string;
    End_Date: string;
    Priority: number = 0;
    Users: Users[];
    Tasks: Tasks[];
    Manager: Users;
    NumberofTasks: number;
    TasksCompleted: number;
}
