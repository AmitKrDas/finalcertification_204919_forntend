import { Users } from './users';
import { ParentTasks } from './parent-tasks';
import { Projects } from './projects';

export class Tasks {
    Parent_Tasks: ParentTasks;
    Projects: Projects;
    Users: Users[];
    Task_ID: number;
    Parent_ID: number;
    Project_ID: number;
    Task: string;
    Start_Date: string;
    End_Date: string;
    Priority = 0;
    Status= 'active';

}
