import { SharedService } from './../../Service/shared.service';
import { Users } from './../../Models/users';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { AddUsersComponent } from './add-users.component';
import { from, empty } from 'rxjs';

describe('AddUsersComponent', () => {
  let component: AddUsersComponent;
  let fixture: ComponentFixture<AddUsersComponent>;  
  let httpClient: HttpClient; 
  let service: SharedService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, HttpClientTestingModule],
      declarations: [ AddUsersComponent ],
      providers: [
        SharedService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUsersComponent);
    service = TestBed.get(SharedService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get all the users', () => {

    const users: Users[] = [
      {
        Projects: null,
        Tasks: null,
        User_ID: 17,
        First_Name: 'Amit',
        Last_Name: 'Da',
        Employee_ID: 'EMP00123',
        Project_ID: null,
        Task_ID: 18
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 41,
          Project: 'testwer',
          Start_Date: null,
          End_Date: null,
          Priority: 0,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: null,
        User_ID: 18,
        First_Name: 'Anuo',
        Last_Name: 'Das',
        Employee_ID: 'EMP002',
        Project_ID: 41,
        Task_ID: null
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 37,
          Project: 'test',
          Start_Date: null,
          End_Date: null,
          Priority: 2,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: {
          Parent_Tasks: {
            Parent_ID: 13,
            Parent_Task: 'Parent Task 1'
          },
          Projects: null,
          Users: [],
          Task_ID: 17,
          Parent_ID: 13,
          Project_ID: null,
          Task: 'Task 1 - p1 upa',
          Start_Date: '2019-12-22T00:00:00',
          End_Date: '2222-01-04T00:00:00',
          Priority: 4,
          Status: 'completed '
        },
        User_ID: 19,
        First_Name: 'Rahul',
        Last_Name: 'Patel',
        Employee_ID: 'EMP003',
        Project_ID: 37,
        Task_ID: 17
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 20,
        First_Name: 'David',
        Last_Name: 'Solomon',
        Employee_ID: 'EMP004',
        Project_ID: 35,
        Task_ID: null
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 21,
        First_Name: 'Tony',
        Last_Name: 'Callaway',
        Employee_ID: 'EMP005',
        Project_ID: 31,
        Task_ID: 22
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 23,
        First_Name: 'Ravi',
        Last_Name: 'Reddy',
        Employee_ID: 'EMP007',
        Project_ID: 34,
        Task_ID: 19
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 24,
        First_Name: 'Ayan-updated',
        Last_Name: 'Sen',
        Employee_ID: 'EMP008',
        Project_ID: 62,
        Task_ID: 21
      }];

    spyOn(service, 'getUsers').and.callFake(() => {
        return from([users]);
      });
    component.getAllUsers();

    expect(component.users).toEqual(users);
  });

  it('should call save new user ', () => {
    component.submitLabel = 'Add';
    const spy = spyOn(service, 'addUser').and.returnValue( empty() );
    component.addUser();
    expect(spy).toHaveBeenCalled();
  });

  it('should call update user ', () => {
    component.submitLabel = 'Update';
    const spy = spyOn(service, 'updateUser').and.returnValue( empty() );
    component.addUser();
    expect(spy).toHaveBeenCalled();
  });

  it('should call delete user ', () => {
    const users = new Users();
    users.Projects = null;
    users.Tasks = null;
    users.User_ID = 17;
    users.First_Name = 'Amit';
    users.Last_Name ='Da';
    users.Employee_ID = 'EMP00123';
    users.Project_ID = null;
    users.Task_ID = 18;
    const spy = spyOn(service, 'deleteUser').and.returnValue( empty() );
    component.deleteUser(users);
    expect(spy).toHaveBeenCalled();
  });

  it('should sort by the users last name', () => {

    const users: Users[] = [
      {
        Projects: null,
        Tasks: null,
        User_ID: 17,
        First_Name: 'Amit',
        Last_Name: 'Da',
        Employee_ID: 'EMP00123',
        Project_ID: null,
        Task_ID: 18
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 41,
          Project: 'testwer',
          Start_Date: null,
          End_Date: null,
          Priority: 0,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: null,
        User_ID: 18,
        First_Name: 'Anuo',
        Last_Name: 'Das',
        Employee_ID: 'EMP002',
        Project_ID: 41,
        Task_ID: null
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 37,
          Project: 'test',
          Start_Date: null,
          End_Date: null,
          Priority: 2,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: {
          Parent_Tasks: {
            Parent_ID: 13,
            Parent_Task: 'Parent Task 1'
          },
          Projects: null,
          Users: [],
          Task_ID: 17,
          Parent_ID: 13,
          Project_ID: null,
          Task: 'Task 1 - p1 upa',
          Start_Date: '2019-12-22T00:00:00',
          End_Date: '2222-01-04T00:00:00',
          Priority: 4,
          Status: 'completed '
        },
        User_ID: 19,
        First_Name: 'Rahul',
        Last_Name: 'Patel',
        Employee_ID: 'EMP003',
        Project_ID: 37,
        Task_ID: 17
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 20,
        First_Name: 'David',
        Last_Name: 'Solomon',
        Employee_ID: 'EMP004',
        Project_ID: 35,
        Task_ID: null
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 21,
        First_Name: 'Tony',
        Last_Name: 'Callaway',
        Employee_ID: 'EMP005',
        Project_ID: 31,
        Task_ID: 22
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 23,
        First_Name: 'Ravi',
        Last_Name: 'Reddy',
        Employee_ID: 'EMP007',
        Project_ID: 34,
        Task_ID: 19
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 24,
        First_Name: 'Ayan-updated',
        Last_Name: 'Sen',
        Employee_ID: 'EMP008',
        Project_ID: 62,
        Task_ID: 21
      }];
    component.users = users;

    component.sortBy('lName');

    expect(component.users.findIndex(x=> x.Employee_ID === 'EMP005')).toEqual(0);
  });

  it('should sort by the users first name', () => {

    const users: Users[] = [
      {
        Projects: null,
        Tasks: null,
        User_ID: 17,
        First_Name: 'Amit',
        Last_Name: 'Da',
        Employee_ID: 'EMP00123',
        Project_ID: null,
        Task_ID: 18
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 41,
          Project: 'testwer',
          Start_Date: null,
          End_Date: null,
          Priority: 0,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: null,
        User_ID: 18,
        First_Name: 'Anuo',
        Last_Name: 'Das',
        Employee_ID: 'EMP002',
        Project_ID: 41,
        Task_ID: null
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 37,
          Project: 'test',
          Start_Date: null,
          End_Date: null,
          Priority: 2,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: {
          Parent_Tasks: {
            Parent_ID: 13,
            Parent_Task: 'Parent Task 1'
          },
          Projects: null,
          Users: [],
          Task_ID: 17,
          Parent_ID: 13,
          Project_ID: null,
          Task: 'Task 1 - p1 upa',
          Start_Date: '2019-12-22T00:00:00',
          End_Date: '2222-01-04T00:00:00',
          Priority: 4,
          Status: 'completed '
        },
        User_ID: 19,
        First_Name: 'Rahul',
        Last_Name: 'Patel',
        Employee_ID: 'EMP003',
        Project_ID: 37,
        Task_ID: 17
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 20,
        First_Name: 'David',
        Last_Name: 'Solomon',
        Employee_ID: 'EMP004',
        Project_ID: 35,
        Task_ID: null
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 21,
        First_Name: 'Tony',
        Last_Name: 'Callaway',
        Employee_ID: 'EMP005',
        Project_ID: 31,
        Task_ID: 22
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 23,
        First_Name: 'Ravi',
        Last_Name: 'Reddy',
        Employee_ID: 'EMP007',
        Project_ID: 34,
        Task_ID: 19
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 24,
        First_Name: 'Ayan-updated',
        Last_Name: 'Sen',
        Employee_ID: 'EMP008',
        Project_ID: 62,
        Task_ID: 21
      }];

    component.users = users;

    component.sortBy('fName');

    expect(component.users.findIndex(x=> x.Employee_ID === 'EMP00123')).toEqual(0);
  });

  it('should sort by the users Employee ID', () => {

    const users: Users[] = [
      {
        Projects: null,
        Tasks: null,
        User_ID: 17,
        First_Name: 'Amit',
        Last_Name: 'Da',
        Employee_ID: 'EMP00123',
        Project_ID: null,
        Task_ID: 18
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 41,
          Project: 'testwer',
          Start_Date: null,
          End_Date: null,
          Priority: 0,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: null,
        User_ID: 18,
        First_Name: 'Anuo',
        Last_Name: 'Das',
        Employee_ID: 'EMP002',
        Project_ID: 41,
        Task_ID: null
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 37,
          Project: 'test',
          Start_Date: null,
          End_Date: null,
          Priority: 2,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: {
          Parent_Tasks: {
            Parent_ID: 13,
            Parent_Task: 'Parent Task 1'
          },
          Projects: null,
          Users: [],
          Task_ID: 17,
          Parent_ID: 13,
          Project_ID: null,
          Task: 'Task 1 - p1 upa',
          Start_Date: '2019-12-22T00:00:00',
          End_Date: '2222-01-04T00:00:00',
          Priority: 4,
          Status: 'completed '
        },
        User_ID: 19,
        First_Name: 'Rahul',
        Last_Name: 'Patel',
        Employee_ID: 'EMP003',
        Project_ID: 37,
        Task_ID: 17
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 20,
        First_Name: 'David',
        Last_Name: 'Solomon',
        Employee_ID: 'EMP004',
        Project_ID: 35,
        Task_ID: null
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 21,
        First_Name: 'Tony',
        Last_Name: 'Callaway',
        Employee_ID: 'EMP005',
        Project_ID: 31,
        Task_ID: 22
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 23,
        First_Name: 'Ravi',
        Last_Name: 'Reddy',
        Employee_ID: 'EMP007',
        Project_ID: 34,
        Task_ID: 19
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 24,
        First_Name: 'Ayan-updated',
        Last_Name: 'Sen',
        Employee_ID: 'EMP008',
        Project_ID: 62,
        Task_ID: 21
      }];

    component.users = users;

    component.sortBy('empID');

    expect(component.users.findIndex(x=> x.Employee_ID === 'EMP00123')).toEqual(0);
  });


});
