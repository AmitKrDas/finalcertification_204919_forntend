import { SharedService } from './../../Service/shared.service';
import { Users } from './../../Models/users';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.css']
})
export class AddUsersComponent implements OnInit {
  user: Users;
  users: Array<Users>;
  submitLabel = 'Add';
  searchInput = '';
  cloneUsers: Array<Users>;
  msg ='';
  showMsg = false;
  msgClass = '';

  constructor(private ss: SharedService) { 
    this.user = new Users();
  }

  ngOnInit() {
    this.getAllUsers();
  }
  getAllUsers() {
    this.ss.getUsers().subscribe(
      (data: Array<Users>) => { 
        this.users = data;
        this.cloneUsers = data;
      },
      (error: any) => {console.log(error); }
    );
  }

  searchUsers() {
    this.users = this.cloneUsers;
    this.users = this.users.filter(x => 
      (x.First_Name.toLowerCase().includes(this.searchInput.toLowerCase())) ||
      (x.Last_Name.toLowerCase().includes(this.searchInput.toLowerCase()))
    );
  }

  sortBy(sortParam: string) {
    if (sortParam === 'fName') {
      this.users.sort((a, b) => a.First_Name.localeCompare(b.First_Name));
    } else if (sortParam === 'lName') {
      this.users.sort((a, b) => a.Last_Name.localeCompare(b.Last_Name));
    } else if (sortParam === 'empID') {
      this.users.sort((a, b) => a.Employee_ID.localeCompare(b.Employee_ID));
    }
  }

  addUser(){
    
    // if (!formAddUser.form.valid) {
    //   return;
    // }
    if (this.submitLabel === 'Add') {
      this.ss.addUser(this.user).subscribe(
        (data: Users) => {
          this.user = data;
          this.msgClass = 'alert-success';
          this.showMsg = true;
          this.msg = 'User was added successfully';
          this.getAllUsers();
        },
        (error: any) => {
          console.log(error);
          this.msgClass = 'alert-danger';
          this.showMsg = true;
          this.msg = 'User was not added';
         }
      );
    } else if(this.submitLabel === 'Update'){
      //this.user.Employee_ID = this.user.Employee_ID.trim();
      this.ss.updateUser(this.user).subscribe(
        () => {
          console.log(`User with ID = ${this.user.User_ID} updated.`);
          this.msgClass = 'alert-success';
          this.showMsg = true;
          this.msg = `User successfully updated.`;
          this.getAllUsers();
        },
        (error: any) => {
          console.log(error);
          this.msgClass = 'alert-danger';
          this.showMsg = true;
          this.msg = 'User was not uodated';
         }
      );
    }
  }

  deleteUser(item: Users){
    this.ss.deleteUser(item.User_ID).subscribe(
      () => {
        console.log(`User with ID = ${item.User_ID} deleted.`);
        this.msgClass = 'alert-success';
        this.showMsg = true;
        this.msg = `User successfully deleted.`;
        this.getAllUsers();
    },
      (error: any) => {
        console.log(error); 
        this.msgClass = 'alert-danger';
        this.showMsg = true;
        this.msg = 'User was not deleted';
      }
    );
  }

  editUser(item: Users){
    this.user = item;
    console.log(this.user);
    this.submitLabel = 'Update';
  }

  resetInputs() {
    this.user = new Users();
    this.submitLabel = 'Add';
  }
}
