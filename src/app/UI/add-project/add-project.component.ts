import { Users } from './../../Models/users';
import { DatePipe } from '@angular/common';
import { SharedService } from './../../Service/shared.service';
import { Component, OnInit } from '@angular/core';
import { Projects } from './../../Models/projects';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';




@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.css']
})
export class AddProjectComponent implements OnInit {
  submitted = false;
  responseJson: string;
  responseSearch: Array<Projects>;
  errorMsg: string;
  searchInput: string;
  project: Projects;
  addBtnLabel = 'Add';
  user: Users;
  users: Array<Users>;
  cloneUsers: Array<Users>;
  orderByCol = 'Project';
  hasStrDtEndDt = false;
  dateError = false;
  filterText = '';
  msg ='';
  showMsg = false;
  msgClass = '';
  datepipe = new DatePipe('en-US');
  public retVal = false;
  


  constructor(private ss: SharedService) {
    this.project = new Projects();
    this.user = new Users();
  }

  ngOnInit() {
    this.getProjectDetails();
    this.getUsers();
  }

  get ProjectsList(): Projects[] {
    return this.responseSearch;
}
  checkDates() {
    if (this.project.End_Date && this.project.Start_Date) {
      this.dateError = this.project.End_Date.localeCompare(this.project.Start_Date) < 1;
    } else{
      this.dateError = false;
    }
  }
 
  toggleDates(e: any){
    this.hasStrDtEndDt = e.target.checked;
    if (e.target.checked) {
      this.project.Start_Date = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
      this.project.End_Date = this.datepipe.transform(new Date().setDate( new Date().getDate() + 1), 'yyyy-MM-dd');
    } else{
      this.project.Start_Date = '';
      this.project.End_Date = '';
    }
  }
  getProjectDetails() {
    this.ss.getAllProjects().subscribe(
      (data) => {
        this.responseSearch = data;
        this.retVal = true;
      },
      (error: any) => {console.log(error); this.retVal = false;}
    );
    this.retVal = true;
  }

  searchProjects() {
    this.ss.searchProjects(this.searchInput).subscribe(
      (data) => {this.responseSearch = data; },
      (error: any) => {console.log(error); }
    );
  }

  addProject(){    
    if (this.addBtnLabel === 'Add') {
      if (this.project.Priority == null) {

        this.project.Priority = 0;
      }
      
      this.ss.addProject(this.project).subscribe(
        (data: Projects) => {
          console.log(data);
          this.msgClass = 'alert-success';
          this.showMsg = true;
          this.msg = 'Project was added successfully';
          this.getProjectDetails();
        },
        (error: any) => { 
          console.log(error); 
          this.msgClass = 'alert-danger';
          this.showMsg = true;
          this.msg = 'Project was not added';
        }
      );
    } else if (this.addBtnLabel === 'Update'){
      console.log(this.project);
      this.ss.updateProject(this.project).subscribe(
        () => {
        this.msgClass = 'alert-success';
        this.showMsg = true;
        this.msg = 'Project was updated successfully';
        this.getProjectDetails();},
        (error: any) => { 
          console.log(error);
          this.msgClass = 'alert-danger';
          this.showMsg = true;
          this.msg = 'Project was not updated';
        }
      );
      //console.log(this.msg);
    }
    //this.resetInputs();
  }
  
  loadProject(item: Projects) {
    item.Start_Date = this.datepipe.transform(item.Start_Date, 'yyyy-MM-dd');
    item.End_Date = this.datepipe.transform(item.End_Date, 'yyyy-MM-dd');    
    this.hasStrDtEndDt = (item.Start_Date !==  '')  && (item.End_Date !== '');    
    this.project = item;
    this.user = new Users();

    if (item.Users.length > 0) {
      this.user = item.Users[0];
    }

    console.log(this.project);
    this.addBtnLabel = 'Update';
  }

  getUsers(){
      this.ss.getUsers().subscribe(
      (data: Users[]) => {
        this.users = data; 
        this.cloneUsers = data;
      },
      (error: any) => { console.log(error); }
    );
  }

  selectManager(item: Users) {
    this.user = item;
    this.user.Project_ID = this.project.Project_ID;
    this.project.Users = [new Users()];
    this.project.Users[0] = (this.user);
    this.filterText = '';
    this.users = this.cloneUsers;
  }

  resetInputs() {
    this.project = new Projects();
    this.project.Priority = 1;
    this.user = new Users();
    this.addBtnLabel = 'Add';
  }

  sortBy(sortParam: string) {
    if (sortParam === 'Project') {
      this.responseSearch.sort((a, b) => a.Project.localeCompare(b.Project));
    } else if (sortParam === 'StartDate') {
      this.responseSearch.sort((a, b) => a.Start_Date.localeCompare(b.Start_Date));
    } else if (sortParam === 'EndDate') {
      this.responseSearch.sort((a, b) => a.End_Date.localeCompare(b.End_Date));
    } else if (sortParam === 'Priority') {
      this.responseSearch.sort((a, b) => a.Priority - b.Priority );
    } else if (sortParam === 'Completed') {
      this.responseSearch.sort((a, b) => a.TasksCompleted - b.TasksCompleted );
    }
  }

  filterUsers() {
    this.users = this.cloneUsers;
    this.users = this.users.filter(x =>
      (x.First_Name.toLowerCase().includes(this.filterText.toLowerCase()))
    );
  }
  
}
