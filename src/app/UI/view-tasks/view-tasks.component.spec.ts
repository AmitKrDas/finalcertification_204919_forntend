import { Projects } from './../../Models/projects';
import { Tasks } from './../../Models/tasks';
import { SharedService } from './../../Service/shared.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTasksComponent } from './view-tasks.component';
import { from, empty } from 'rxjs';

describe('ViewTasksComponent', () => {
  let component: ViewTasksComponent;
  let fixture: ComponentFixture<ViewTasksComponent>;
  let service: SharedService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule, HttpClientTestingModule],
      declarations: [ ViewTasksComponent ],
      providers: [
        SharedService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTasksComponent);
    service = TestBed.get(SharedService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get all the tasks', () => {

    const tasks: Tasks[] = [{
      Parent_Tasks: {
        Parent_ID: 13,
        Parent_Task: 'Parent Task 1'
      },
      Projects: null,
      Users: null,
      Task_ID: 17,
      Parent_ID: 13,
      Project_ID: null,
      Task: 'Task 1 - p1 upa',
      Start_Date: '2019-12-22T00:00:00',
      End_Date: '2222-01-04T00:00:00',
      Priority: 4,
      Status: 'completed '
    },
    {
      Parent_Tasks: {
        Parent_ID: 14,
        Parent_Task: 'Parent Task 2'
      },
      Projects: null,
      Users: [
        {
          Projects: null,
          User_ID: 17,
          First_Name: 'Amit',
          Last_Name: 'Da',
          Employee_ID: 'EMP00123',
          Project_ID: null,
          Task_ID: 18,
          Tasks: null
        }
      ],
      Task_ID: 18,
      Parent_ID: 14,
      Project_ID: null,
      Task: 'Task2',
      Start_Date: '2019-12-22T00:00:00',
      End_Date: '2020-01-16T00:00:00',
      Priority: 2,
      Status: 'completed '
    },
    {
      Parent_Tasks: null,
      Projects: null,
      Users: [
        {
          Projects: null,
          User_ID: 23,
          First_Name: 'Ravi',
          Last_Name: 'Reddy',
          Employee_ID: 'EMP007',
          Project_ID: 34,
          Task_ID: 19,
          Tasks: null
        }
      ],
      Task_ID: 19,
      Parent_ID: null,
      Project_ID: 32,
      Task: 'Task1-P2',
      Start_Date: '2019-12-25T00:00:00',
      End_Date: '2020-02-14T00:00:00',
      Priority: 3,
      Status: 'completed '
    },
    {
      Parent_Tasks: null,
      Projects: null,
      Users: [],
      Task_ID: 20,
      Parent_ID: null,
      Project_ID: 32,
      Task: 'Task3 - P2',
      Start_Date: '2019-12-24T00:00:00',
      End_Date: '2020-01-15T00:00:00',
      Priority: 8,
      Status: 'completed '
    }
  ];
    spyOn(service, 'getAllTasks').and.callFake(() => {
        return from([tasks]);
      });
    component.getAllTasks();

    expect(component.tasks).toEqual(tasks);
  });

  it('should filter task based on project selection', () => {

    const tasks: Tasks[] = [{
      Parent_Tasks: {
        Parent_ID: 13,
        Parent_Task: 'Parent Task 1'
      },
      Projects: null,
      Users: null,
      Task_ID: 17,
      Parent_ID: 13,
      Project_ID: null,
      Task: 'Task 1 - p1 upa',
      Start_Date: '2019-12-22T00:00:00',
      End_Date: '2222-01-04T00:00:00',
      Priority: 4,
      Status: 'completed '
    },
    {
      Parent_Tasks: {
        Parent_ID: 14,
        Parent_Task: 'Parent Task 2'
      },
      Projects: null,
      Users: [
        {
          Projects: null,
          User_ID: 17,
          First_Name: 'Amit',
          Last_Name: 'Da',
          Employee_ID: 'EMP00123',
          Project_ID: null,
          Task_ID: 18,
          Tasks: null
        }
      ],
      Task_ID: 18,
      Parent_ID: 14,
      Project_ID: null,
      Task: 'Task2',
      Start_Date: '2019-12-22T00:00:00',
      End_Date: '2020-01-16T00:00:00',
      Priority: 2,
      Status: 'completed '
    },
    {
      Parent_Tasks: null,
      Projects: null,
      Users: [],
      Task_ID: 20,
      Parent_ID: null,
      Project_ID: 32,
      Task: 'Task3 - P2',
      Start_Date: '2019-12-24T00:00:00',
      End_Date: '2020-01-15T00:00:00',
      Priority: 8,
      Status: 'completed '
    }
  ];

    const project: Projects = new Projects();
    project.Tasks = null;
    project.Users = null;
    project.Project_ID = 32;
    project.Project = 'Proj2';
    project.Start_Date = '2019-12-26T00:00:00';
    project.End_Date = '2020-06-26T00:00:00';
    project.Priority = 12;
    project.NumberofTasks = 3;
    project.TasksCompleted = 3;
    project.Manager = null;

    const filteredTasks: Tasks[] = [
    {
      Parent_Tasks: null,
      Projects: null,
      Users: [],
      Task_ID: 20,
      Parent_ID: null,
      Project_ID: 32,
      Task: 'Task3 - P2',
      Start_Date: '2019-12-24T00:00:00',
      End_Date: '2020-01-15T00:00:00',
      Priority: 8,
      Status: 'completed '
    }
  ];

    spyOn(service, 'getAllTasks').and.callFake(() => {
        return from([tasks]);
      });
    component.getAllTasks();
    component.filterTask(project);

    expect(component.tasks).toEqual(filteredTasks);
  });

  it('should call completeTask task', () => {
    const tasktoComplete: Tasks = new Tasks();
    tasktoComplete.Parent_Tasks = null;
    tasktoComplete.Projects = null;
    tasktoComplete.Users = [];
    tasktoComplete.Task_ID = 20;
    tasktoComplete.Parent_ID = null;
    tasktoComplete.Project_ID = 32;
    tasktoComplete.Task = 'Task3 - P2';
    tasktoComplete.Start_Date = '2019-12-24T00:00:00';
    tasktoComplete.End_Date = '2020-01-15T00:00:00';
    tasktoComplete.Priority = 8;
    tasktoComplete.Status = 'active';
      
    const spy = spyOn(service, 'updateTask').and.returnValue( empty() );

    component.completeTask(tasktoComplete);

    expect(spy).toHaveBeenCalled();
  });

});
