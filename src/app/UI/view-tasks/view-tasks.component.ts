import { Router, ActivatedRoute} from '@angular/router';
import { Projects } from './../../Models/projects';
import { SharedService } from './../../Service/shared.service';
import { Tasks } from './../../Models/tasks';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-tasks',
  templateUrl: './view-tasks.component.html',
  styleUrls: ['./view-tasks.component.css']
})
export class ViewTasksComponent implements OnInit {
  task: Tasks;
  tasks: Array<Tasks>;
  cloneTasks: Array<Tasks>;
  projects: Array<Projects>;
  cloneProjects: Array<Projects>;
  filterText: string;
  projectName: string;
  msg ='';
  showMsg = false;
  msgClass = '';


  constructor(private ss: SharedService,  private router: Router) {
    this.task = new Tasks();
  }

  ngOnInit() {
    this.getAllTasks();
    this.getAllProjects();
  }
  editTask(item: Tasks){
    this.router.navigate(['/addTask', { id: item.Task_ID }]);
  }
  getAllProjects() {
    this.ss.getAllProjects().subscribe(
      (data) => {
        this.projects = data;
        this.cloneProjects = data;
      },
      (error: any) => {console.log(error); }
    );
  }
  filterProjects(){
    this.projects = this.cloneProjects;
    this.projects = this.projects.filter(x =>
      (x.Project.toLowerCase().includes(this.filterText.toLowerCase()))
    );
  }
  getAllTasks(){
    this.ss.getAllTasks().subscribe(
      (data: Array<Tasks>) => {
        this.tasks = data;
        this.cloneTasks = data;
      },
      (error: any) => {console.log(error); }
    );
  }

  filterTask(item: Projects){
    this.tasks = this.cloneTasks;
    this.tasks = this.tasks.filter(x =>
      (x.Project_ID === item.Project_ID)
      );
    this.projectName = item.Project;
  }
  sortBy(sortParam: string) {
    if (sortParam === 'sDate') {
      this.tasks.sort((a, b) => a.Start_Date.localeCompare(b.Start_Date));
    } else if (sortParam === 'eDate') {
      this.tasks.sort((a, b) => a.End_Date.localeCompare(b.End_Date));
    } else if (sortParam === 'priority') {
      this.tasks.sort((a, b) => a.Priority - b.Priority);
    } else if (sortParam === 'status') {
      this.tasks.sort((a, b) => a.Status.localeCompare(b.Status));
    }
  }

  completeTask(item: Tasks){
    item.Status = 'completed';
    this.ss.updateTask(item).subscribe(
      () => {
        console.log(`Task with task ID =${item.Task_ID} marked as completed`);
        this.msgClass = 'alert-success';
        this.showMsg = true;
        this.msg = `Task : ${item.Task} marked as completed`;
        this.getAllTasks();},
      (err) => {
        console.log(err); 
        this.msgClass = 'alert-danger';
        this.showMsg = true;
        this.msg = 'Task was not marked as complete';
      }
    );
  }


}
