import { Projects } from './../../Models/projects';
import { Users } from './../../Models/users';
import { Tasks } from './../../Models/tasks';
import { SharedService } from './../../Service/shared.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTaskComponent } from './add-task.component';
import { from, empty } from 'rxjs';

describe('AddTaskComponent', () => {
  let component: AddTaskComponent;
  let fixture: ComponentFixture<AddTaskComponent>;
  let httpClient: HttpClient;
  let service: SharedService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, HttpClientTestingModule, RouterTestingModule],
      declarations: [ AddTaskComponent ],
      providers: [
        SharedService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaskComponent);
    service = TestBed.get(SharedService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
  it('should call save new task', () => {
    component.submitLabel = 'Add';
    component.isParent = false;
    const spy = spyOn(service, 'addTask').and.returnValue( empty() );
    component.submitTask();

    expect(spy).toHaveBeenCalled();
  });
  it('should call save new parent task', () => {
    component.submitLabel = 'Add';
    component.isParent = true;
    const spy = spyOn(service, 'addParentTask').and.returnValue( empty() );
    component.submitTask();

    expect(spy).toHaveBeenCalled();
  });

  it('should call update task', () => {
    component.submitLabel = 'Update';
    const spy = spyOn(service, 'updateTask').and.returnValue( empty() );

    component.submitTask();

    expect(spy).toHaveBeenCalled();
  });

  it('should get all the users', () => {

    const users: Users[] = [
      {
        Projects: null,
        Tasks: null,
        User_ID: 17,
        First_Name: 'Amit',
        Last_Name: 'Da',
        Employee_ID: 'EMP00123',
        Project_ID: null,
        Task_ID: 18
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 41,
          Project: 'testwer',
          Start_Date: null,
          End_Date: null,
          Priority: 0,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: null,
        User_ID: 18,
        First_Name: 'Anuo',
        Last_Name: 'Das',
        Employee_ID: 'EMP002',
        Project_ID: 41,
        Task_ID: null
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 37,
          Project: 'test',
          Start_Date: null,
          End_Date: null,
          Priority: 2,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: {
          Parent_Tasks: {
            Parent_ID: 13,
            Parent_Task: 'Parent Task 1'
          },
          Projects: null,
          Users: [],
          Task_ID: 17,
          Parent_ID: 13,
          Project_ID: null,
          Task: 'Task 1 - p1 upa',
          Start_Date: '2019-12-22T00:00:00',
          End_Date: '2222-01-04T00:00:00',
          Priority: 4,
          Status: 'completed '
        },
        User_ID: 19,
        First_Name: 'Rahul',
        Last_Name: 'Patel',
        Employee_ID: 'EMP003',
        Project_ID: 37,
        Task_ID: 17
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 20,
        First_Name: 'David',
        Last_Name: 'Solomon',
        Employee_ID: 'EMP004',
        Project_ID: 35,
        Task_ID: null
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 21,
        First_Name: 'Tony',
        Last_Name: 'Callaway',
        Employee_ID: 'EMP005',
        Project_ID: 31,
        Task_ID: 22
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 23,
        First_Name: 'Ravi',
        Last_Name: 'Reddy',
        Employee_ID: 'EMP007',
        Project_ID: 34,
        Task_ID: 19
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 24,
        First_Name: 'Ayan-updated',
        Last_Name: 'Sen',
        Employee_ID: 'EMP008',
        Project_ID: 62,
        Task_ID: 21
      }];

    spyOn(service, 'getUsers').and.callFake(() => {
        return from([users]);
      });
    component.getAllUsers();

    expect(component.users).toEqual(users);
  });

  it('should filter the users', () => {

    const users: Users[] = [
      {
        Projects: null,
        Tasks: null,
        User_ID: 17,
        First_Name: 'Amit',
        Last_Name: 'Da',
        Employee_ID: 'EMP00123',
        Project_ID: null,
        Task_ID: 18
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 41,
          Project: 'testwer',
          Start_Date: null,
          End_Date: null,
          Priority: 0,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: null,
        User_ID: 18,
        First_Name: 'Anuo',
        Last_Name: 'Das',
        Employee_ID: 'EMP002',
        Project_ID: 41,
        Task_ID: null
      },
      {
        Projects: {
          Tasks: [],
          Users: [],
          Project_ID: 37,
          Project: 'test',
          Start_Date: null,
          End_Date: null,
          Priority: 2,
          NumberofTasks: 0,
          TasksCompleted: 0,
          Manager: null
        },
        Tasks: {
          Parent_Tasks: {
            Parent_ID: 13,
            Parent_Task: 'Parent Task 1'
          },
          Projects: null,
          Users: [],
          Task_ID: 17,
          Parent_ID: 13,
          Project_ID: null,
          Task: 'Task 1 - p1 upa',
          Start_Date: '2019-12-22T00:00:00',
          End_Date: '2222-01-04T00:00:00',
          Priority: 4,
          Status: 'completed '
        },
        User_ID: 19,
        First_Name: 'Rahul',
        Last_Name: 'Patel',
        Employee_ID: 'EMP003',
        Project_ID: 37,
        Task_ID: 17
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 20,
        First_Name: 'David',
        Last_Name: 'Solomon',
        Employee_ID: 'EMP004',
        Project_ID: 35,
        Task_ID: null
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 21,
        First_Name: 'Tony',
        Last_Name: 'Callaway',
        Employee_ID: 'EMP005',
        Project_ID: 31,
        Task_ID: 22
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 23,
        First_Name: 'Ravi',
        Last_Name: 'Reddy',
        Employee_ID: 'EMP007',
        Project_ID: 34,
        Task_ID: 19
      },
      {
        Projects: null,
        Tasks: null,
        User_ID: 24,
        First_Name: 'Ayan-updated',
        Last_Name: 'Sen',
        Employee_ID: 'EMP008',
        Project_ID: 62,
        Task_ID: 21
      }];


    const filterUser: Users[] = [
      {
      Projects: null,
      Tasks: null,
      User_ID: 24,
      First_Name: 'Ayan-updated',
      Last_Name: 'Sen',
      Employee_ID: 'EMP008',
      Project_ID: 62,
      Task_ID: 21
    }];

    spyOn(service, 'getUsers').and.callFake(() => {
        return from([users]);
      });
    component.getAllUsers();
    component.filterText = 'Ayan-updated';
    component.filterUsers();
    expect(component.users).toEqual(filterUser);
  });

  it('should filter the projects', () => {

    const projects: Projects[] = [
      {
      Tasks: null,
      Users: null,
      Project_ID: 31,
      Project: 'Vantage',
      Start_Date: '2019-12-22T00:00:00',
      End_Date: '2020-01-31T00:00:00',
      Priority: 4,
      NumberofTasks: 0,
      TasksCompleted: 0,
      Manager: null
    },
    {
      Tasks: null,
      Users: null,
      Project_ID: 32,
      Project: 'SiteSolution',
      Start_Date: '2019-12-26T00:00:00',
      End_Date: '2020-06-26T00:00:00',
      Priority: 12,
      NumberofTasks: 3,
      TasksCompleted: 3,
      Manager: null
    }
    ];


    const filterProjects: Projects[] = [
      {
        Tasks: null,
        Users: null,
        Project_ID: 32,
        Project: 'SiteSolution',
        Start_Date: '2019-12-26T00:00:00',
        End_Date: '2020-06-26T00:00:00',
        Priority: 12,
        NumberofTasks: 3,
        TasksCompleted: 3,
        Manager: null
      }];

    spyOn(service, 'getAllProjects').and.callFake(() => {
        return from([projects]);
      });
    component.getAllProjects();
    component.filterText = 'SiteSolution';
    component.filterProjects();
    expect(component.projects).toEqual(filterProjects);
  });

});
