import { DatePipe } from '@angular/common';
import { ParentTasks } from './../../Models/parent-tasks';
import { Tasks } from './../../Models/tasks';
import { Projects } from './../../Models/projects';
import { SharedService } from './../../Service/shared.service';
import { Users } from './../../Models/users';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  user: Users;
  users: Array<Users>;
  cloneUsers: Array<Users>;
  project: Projects;
  projects: Array<Projects>;
  cloneProjects: Array<Projects>;
  filterText: string;
  task: Tasks;
  tasks: Array<Tasks>;
  parentTask: ParentTasks;
  newParentTask: ParentTasks;
  cloneParentTasks: Array<ParentTasks>;
  parentTasks: Array<ParentTasks>;
  isParent = false;
  submitLabel = 'Add';
  dateError = false;
  isUpdate = false;
  msg ='';
  showMsg = false;
  msgClass = '';
  datepipe = new DatePipe('en-US');
  
  constructor(private ss: SharedService, private router: Router, private route: ActivatedRoute) {
    this.resetInputs();
  }

  ngOnInit() {
    this.getAllProjects();
    this.getAllUsers();
    this.getAllParentTasks(); 
    let id = this.route.snapshot.paramMap.get('id');
    if(+id > 0){
      this.getTasks(+id);
    }
    
    console.log(id);
  }
  getTasks(taskID: number){
    this.ss.getTasks(taskID).subscribe(
      (data: Tasks) => {
        this.task = data;
        this.project = this.task.Projects;
        if (this.task.Parent_Tasks) {
          this.parentTask = this.task.Parent_Tasks;
        }
        if (this.task.Users.length > 0) {
          this.user = this.task.Users[0];
        }
        this.task.Start_Date = this.datepipe.transform(this.task.Start_Date, 'yyyy-MM-dd');
        this.task.End_Date = this.datepipe.transform(this.task.End_Date, 'yyyy-MM-dd');
        this.submitLabel = 'Update';
        this.isUpdate = true;
      },
      (error: any) => {console.log(error);}
    );
  }
  submitTask(){
    if (this.project.Priority == null) {

      this.project.Priority = 0;
    }
    if (this.submitLabel === 'Add' && !this.isParent) {
      this.ss.addTask(this.task).subscribe(
        (data) => {
          console.log(`Task added successfully: Task ID: ${data.Task_ID}`)
          this.router.navigate(['/viewTasks']);
      },
        (error) => { console.log(error);}
      );
    } else if(this.submitLabel === 'Add' && this.isParent) {
      this.newParentTask = new ParentTasks();
      this.newParentTask.Parent_Task = this.task.Task;
      this.ss.addParentTask(this.newParentTask).subscribe(
        (data) => {
          console.log(`Parent Task added successfully: Task ID: ${data.Parent_ID}`);
          this.router.navigate(['/viewTasks']);
        },
        (error) => { console.log(error); }
      );
    } else if (this.submitLabel === 'Update') {
      this.ss.updateTask(this.task).subscribe(
        () => {this.router.navigate(['/viewTasks']);},
        (err) => { console.log(err);}
      );
    }
  }
  resetInputs(){
    this.user = new Users();
    this.project = new Projects();
    this.task = new Tasks();
    this.parentTask = new ParentTasks();
  }
  checkDates() {
    if (this.task.End_Date && this.task.Start_Date) {
      this.dateError = this.task.End_Date.localeCompare(this.task.Start_Date) < 1;
    } else{
      this.dateError = false;
    }
  }
  changeCheck(checked: boolean){
    this.isParent = checked;
  }
  getAllProjects() {
    this.ss.getAllProjects().subscribe(
      (data) => {
        this.projects = data;
        this.cloneProjects = data;
      },
      (error: any) => {console.log(error); }
    );
  }
  filterProjects(){
    this.projects = this.cloneProjects;
    this.projects = this.projects.filter(x =>
      (x.Project.toLowerCase().includes(this.filterText.toLowerCase()))
    );
  }
  
  selectProject(item: Projects){
    this.project = item;
    this.task.Project_ID = this.project.Project_ID;
    this.filterText = '';
    this.projects = this.cloneProjects;
  }

  getAllParentTasks() {
    this.ss.getAllParentTasks().subscribe(
      (data) => {
        this.parentTasks = data;
        this.cloneParentTasks = data;
      },
      (error: any) => {console.log(error); }
    );
  }
  filterParentTasks() {
    this.parentTasks = this.cloneParentTasks;
    this.parentTasks = this.parentTasks.filter(x =>
      (x.Parent_Task.toLowerCase().includes(this.filterText.toLowerCase()))
    );
  }
  selectParentTasks(item: ParentTasks) {
    this.parentTask = item;
    console.log(this.parentTask);
    this.task.Parent_ID = item.Parent_ID;
    this.filterText = '';
    this.parentTasks = this.cloneParentTasks;
  }

  getAllUsers() {
    this.ss.getUsers().subscribe(
      (data) => {
        this.users = data;
        this.cloneUsers = data;
      },
      (error: any) => {console.log(error); }
    );
  }
  filterUsers() {
    this.users = this.cloneUsers;
    this.users = this.users.filter(x =>
      (x.First_Name.toLowerCase().includes(this.filterText.toLowerCase()))
    );
  }
  selectUsers(item: Users) {
    this.user = item;
    this.task.Users = [new Users()];
    this.task.Users[0] = (this.user);
    this.filterText = '';
    this.users = this.cloneUsers;
  }

}
