import { FormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SharedService } from './shared.service';
import { HttpErrorHandler } from '../http-error-handler.service';
import { MessageService } from '../message.service';

describe('SharedService', () => {

  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: SharedService;
  beforeEach( () => { TestBed.configureTestingModule ({
    imports: [ HttpClientTestingModule, FormsModule ],
    providers: [
      SharedService,
      HttpErrorHandler,
      MessageService
    ]
  });
                      httpClient = TestBed.get(HttpClient);
                      httpTestingController = TestBed.get(HttpTestingController);
                      service = TestBed.get(SharedService);
});

  afterEach(() => {
      // After every test, assert that there are no more pending requests.
      httpTestingController.verify();
    });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
