import { ParentTasks } from './../Models/parent-tasks';
import { Tasks } from './../Models/tasks';
import { Users } from './../Models/users';
import { Projects } from './../Models/projects';
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  apiURL = 'http://localhost:8088/api';
  constructor(private http: HttpClient) { }

  getAllProjects(): Observable<any> {
    return this.http.get(`${this.apiURL}/Projects`);
  }

  searchProjects(searchTxt: string): Observable<any> {
    return this.http.get(`${this.apiURL}/Projects/Search/${searchTxt}`);
  }

  addProject(proj: Projects): Observable<Projects>{
    return this.http.post<Projects>(`${this.apiURL}/Projects`, proj, {
      headers: new HttpHeaders({
        'Content-Type' : 'application/json'
      })
    });
  }

  updateProject(proj: Projects): Observable<void>{
    return this.http.put<void>(`${this.apiURL}/Projects/${proj.Project_ID}`, proj,{
       headers: new HttpHeaders({
        'Content-Type' : 'application/json'
       })
    });
  }

  getUsers(): Observable<Array<Users>> {
    return this.http.get<Array<Users>>(`${this.apiURL}/Users`);
  }

  addUser(user: Users): Observable<Users>{
    return this.http.post<Users>(`${this.apiURL}/Users`, user,{
      headers: new HttpHeaders({
        'Content-Type' : 'application/json'
      })
    });
  }

  updateUser(user: Users): Observable<void>{
    return this.http.put<void>(`${this.apiURL}/Users/${user.User_ID}`, user, {
       headers: new HttpHeaders({
        'Content-Type' : 'application/json'
       })
    });
  }

  deleteUser(userID: number): Observable<Users>{
    return this.http.delete<Users>(`${this.apiURL}/Users/${userID}`);
    }

    getAllTasks(): Observable<Array<Tasks>>{
    return this.http.get<Array<Tasks>>(`${this.apiURL}/Tasks`);
    }

    getTasks(taskID: number): Observable<Tasks>{
      return this.http.get<Tasks>(`${this.apiURL}/Tasks/${taskID}`);
      }

    updateTask(task: Tasks): Observable<void>{
      return this.http.put<void>(`${this.apiURL}/Tasks/${task.Task_ID}`, task, {
         headers: new HttpHeaders({
          'Content-Type' : 'application/json'
         })
      });
    }

    getAllParentTasks(): Observable<Array<ParentTasks>> {
      return this.http.get<Array<ParentTasks>>(`${this.apiURL}/Parent_Tasks`);
      }

      addTask(task: Tasks): Observable<Tasks>{
        return this.http.post<Tasks>(`${this.apiURL}/Tasks`, task,{
          headers: new HttpHeaders({
            'Content-Type' : 'application/json'
          })
        });
      }

      addParentTask(parentTask: ParentTasks): Observable<ParentTasks>{
        return this.http.post<ParentTasks>(`${this.apiURL}/Parent_Tasks`, parentTask,{
          headers: new HttpHeaders({
            'Content-Type' : 'application/json'
          })
        });
      }
}

